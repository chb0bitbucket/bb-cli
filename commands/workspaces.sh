ls() {
  local repo="${1}"
  local url="https://api.bitbucket.org/2.0/repositories"
  _hook_ls() {
    for team in $(teams); do
      if [ -z "${repo}" ]; then
        mkurls "$url/${team}/" |
          xargs -n 30 curl -fsnL | jq -er .values[].links.hooks.href |
          xargs -n 30 curl -fsnL | jq -re .values[].links.self.href |
          xargs -n 30 -P 20 curl -fsnL |
          jq -re 'select(.active) | "\(.url) \(.links.self.href)"'
      else
        # for simplicity sake, assume no more than 100 hooks to a repo. 100 is their internal limit
        mkurls "$url/${team}/$repo/hooks/" |
          jq -re .values[].links.self.href |
          xargs curl -fsnL | jq -re 'select(.active) | "\(.url) \(.links.self.href)"'
      fi
    done

  }
  cache _hook_ls "$repo"
} && export -f ls

mkhook() {
  local workspace=${1:?You must supply a worspace}


  local HOOK=${2:?No hook url suplied}
  local DESCRIPTION=${3:-By ${USER:-${USERNAME:-${HOSTNAME}}} on $(date "+%Y-%m-%d")}
  local EVENTS=${4:+$(echo -e "$(printf '"%s",' "${@:4}")\n" | sed 's/.$//')}
  EVENTS=${EVENTS:-$(printf '"pullrequest:updated", "pullrequest:created", "repo:push"')}
  (
    curl -fsnL -X POST -H "Content-Type: application/json" -H "Accept: application/json" \
      "https://api.bitbucket.org/2.0/workspaces/${workspace}//hooks" -d @- <<EOF
     {
      "description": "${DESCRIPTION}",
      "url": "$HOOK",
      "active": true,
      "events": [$EVENTS]
    }
EOF
  ) | jq -re .links.self.href
} && export -f mk


ls() {
  local repo=${1:?You must supply a repo for branch searching}
  _branch_data "${repo}" | jq -re .values[].links.self.href
} && export -f ls

_branch_data() {
  local repo="${1:?You must supply a repo}"
  _branch_data_() {
    for team in $(teams); do
      mkurls "https://api.bitbucket.org/2.0/repositories/${team}/${repo}/refs/branches/" | xargs -n 30 curl -fsnL
    done
  }
  cache _branch_data_ "${repo}"
}
report() {
  local selection='.values | sort_by(.target.date) | .[] | [$repo, .target.date, .name, .target.author.raw, .links.self.href]'

  local format='sh'
  local repos=()

  while getopts r: opt; do
    case "${opt}" in
      r)
        repos+=("${OPTARG}")
        ;;
      *)
        echo "invalid arg ${opt} supplied. Ignoring" > /dev/stderr
        ;;
    esac
  done
  for repo in "${repos[@]}"; do
    _branch_data "${repo}" | jq -re --arg repo "$repo" "${selection} | @${format}"
  done
}

rm() {

  local repo=${1:?You must supply a repo}
  [ "$#" -eq 1 ] || (echo "You must supply at least 1 branch" >/dev/stderr && return 1)
    teams | xargs -n 1 -I {} printf "https://api.bitbucket.org/2.0/repositories/%s/%s/refs/branches/%s\n" {} "$repo" "${@:2}" \
      | xargs -L 1 -P 20 curl -fsnL -X DELETE

}

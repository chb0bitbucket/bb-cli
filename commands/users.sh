# example usage: bbusers | awk -F ',' '{print $1}'
# apparently you can't delete user from API key
_raw_data() {
  local teams=("$(teams)")

  _user_ls() {
    for team in "${teams[@]}"; do
      mkurls "https://api.bitbucket.org/2.0/teams/${team}/members/" |
        xargs -n 30 -P 20 curl -fsnL | jq -s .
    done
  }

  cache _user_ls
}
help() {
  echo "sample usage
  hooks > jenkins1-hooks.txt
  cat jenkins1-hooks.txt | xargs -I {} -P 20 bash -c 'rmhook \$1' _ {}
  Takes advantage of the fact the repo is in the hook url so we can create new hooks
  cat jenkins1-hooks.txt | sed 's|.*/.*/\(.*\)/hooks/.*|\1|g' | sort -u \\
      | xargs -P 20 -I {} bash -c 'mkhook \$1 https://build.dev.backgroundcheck.com/bitbucket-scmsource-hook/notify' _ {}

  hooks > searchlater.txt
  grep -f thingslookingfor.txt searchlater.txt > hooksiwant_to_delete.txt
  cat hooksiwant_to_delete.txt | awk '{print \$2}' | xargs -P 20 curl -fsnL -X DELETE
  cat hooksiwant_to_delete.txt | sed  's|.*/twengg/\(.*\)/hooks/.*|\1|g' \\
    | xargs -P 40  bash -c 'mkhook \$1 \"https://build.dev.backgroundcheck.com/bitbucket-scmsource-hook/notify\"' _"
}
ls() {
  local selections='.[].values[] | [.uuid, .display_name, .created_on,  .links.self.href]'

  local format=csv
  while (("$#")); do
    case "$1" in
    -f | --format)
      format="${2}"
      shift 2
      ;;
    *) # preserve positional arguments
      PARAMS="$PARAMS $1"
      shift
      ;;
    esac
  done
  eval set -- "$PARAMS"
  echo 'uuid, name, created_on, ref'
  _raw_data | jq -er "${selections} | @$format" | sort
} && export -f ls

ls() {
  declare -A types=(
    [ssh]='.values[].links.clone | map(select(.name == "ssh")) | .[0].href'
    [http]='.values[].links.clone | map(select(.name == "https")) | .[0].href | match(".*bitbucket.org/(.*)") | "https://bitbucket.org/\(.captures[0].string)"'
    [https]='.values[].links.clone | map(select(.name == "https")) | .[0].href | match(".*bitbucket.org/(.*)") | "https://bitbucket.org/\(.captures[0].string)"'
    [simple]='.values[].slug'
  )
  local format=${types['simple']}

  while (("$#")); do
    case "$1" in
    -f | --format)
      format=${types[$2]}
      shift 2
      ;;
    *) # preserve positional arguments
      PARAMS="$PARAMS $1"
      shift
      ;;
    esac
  done
  eval set -- "$PARAMS"
  repo_data | jq -er "$format" | sort
} && export -f ls

repo_data() {
  _repo_data() {
    local teams=("$(teams)")
    for team in "${teams[@]}"; do
      mkurls "https://api.bitbucket.org/2.0/repositories/${team}/" | xargs -n 30 -P 20 curl -fsnL
    done
  }
  cache _repo_data
}
report() {
  echo "updated_on, slug, mainbranch"
  repo_data | jq -er '.values[] | "\(.updated_on), \(.slug), \(.mainbranch.name)"' | sort
} && export -f report

FROM bash
RUN apk add --update curl jq expect-dev && rm -rf /var/cache/apk/*
RUN adduser --disabled-password scm && chown -R scm:scm /home/scm
USER scm
WORKDIR /home/scm
COPY ./bb ./
COPY ./commands ./
VOLUME /home/scm/.cache
ENV PATH=${PATH}:.

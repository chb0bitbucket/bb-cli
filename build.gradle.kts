buildscript {
    repositories {
        mavenCentral()
    }
    dependencies {
        //        '', name: '', version: ''
        "classpath"(group = "javax.xml.bind", name = "jaxb-api", version = "2.3.1")
    }
}

plugins {
    id("com.palantir.docker") version "0.25.0"
}
docker {
    name = "chb0docker/scm:latest
}

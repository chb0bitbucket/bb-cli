#!/usr/bin/env bash
set -e
set -o pipefail

command -v curl 2 &>/dev/null || (echo "You must install curl to proceed" >/dev/stderr && exit 1)
command -v jq 2 &>/dev/null || (echo "You must install jq to proceed" >/dev/stderr && exit 1)
command -v sed 2 &>/dev/null || (echo "You must install sed to proceed" >/dev/stderr && exit 1)
command -v awk 2 &>/dev/null || (echo "You must install awk to proceed" >/dev/stderr && exit 1)

HOME=${HOME:-"$(echo ~)"}

if [ ! -f "$HOME/.netrc" ] && [ ! -f "$HOME/_netrc" ]; then
  echo "You must have a $HOME/.netrc file to use this script. on *unix or $HOME/_netrc on windows. Open passwords are bad!" >/dev/stderr
  echo "See: https://ec.haxx.se/usingcurl/usingcurl-netrc"
  exit 1
fi
test -d "$HOME/.cache/bb" || echo "No cache found at '$HOME/.cache/bb' - one will be created" > /dev/stderr

cache() {
  local file
  test -d "$HOME/.cache/bb" || mkdir -p "$HOME/.cache/bb"
  file="$HOME/.cache/bb/$(printf "%s" "${@}")"
  if [[ -f "${file}" && $(($(date +%s) - $(date -r "$file" +%s))) -le 1800 ]]; then
    cat "${file}"
  else
    "${@}" | tee "${file}"
  fi
}
mkurls() {
  local url="${1:?You must supply a URL}"
  local batchSize=${2:-100}
  local repoCount
  repoCount=$(curl -fsnL "$url?pagelen=0&page=1&fields=size" | jq -re .size)
  local pages=$(((repoCount / batchSize) + 1))

  printf "$url?pagelen=${batchSize}&page=%s\n" $(seq 1 "$pages")
}

teams() {
  _teams(){
    curl -snL https://api.bitbucket.org/2.0/teams/?role=member | jq -re .values[].username
  }
  cache _teams
}
search() {
  local query="${1:?No query provided}"
  local teams=($(teams))
  #  local team=$(curl -snL https://api.bitbucket.org/2.0/teams/?role=member | jq -re .values[].username)
  for team in "${teams[@]}"; do
    curl -fsnL "https://api.bitbucket.org/2.0/teams/$team/search/code?search_query='${query}'" |
      jq -r .values[].file.links.self.href | sed 's|.*/repositories/\(.*\)/src.*|git@bitbucket.org:\1.git|g'
  done

}


if [[ $# == 0 ]]; then
  echo "Available commands:"
  find "$(dirname $BASH_SOURCE)/commands/" -name '*.sh' -print0 | xargs -I {} -0 basename {} .sh
  exit 1
fi
source "$(dirname $BASH_SOURCE)/commands/${1}.sh"
shift
"${@}"
